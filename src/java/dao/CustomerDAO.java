/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 *
 * @author Hoang
 */
public class CustomerDAO {

    public boolean registerUser(String username, String password) {
        Connection connection = DBContext.getConnection();
        PreparedStatement statement = null;

        try {
            // Tạo truy vấn SQL để thêm người dùng vào cơ sở dữ liệu
            String sql = "INSERT INTO customers (username, password) VALUES (?, ?)";
            String hashedPassword = hashPassword(password);

            // Thực hiện truy vấn SQL với các thông tin của người dùng
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, hashedPassword);

            // Thực hiện truy vấn
            int rowsAffected = statement.executeUpdate();

            // Kiểm tra xem truy vấn đã thực hiện thành công hay không
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Băm mật khẩu bằng BCrypt
    private String hashPassword(String rawPassword) {
        BCrypt.Hasher hasher = BCrypt.withDefaults();
        return hasher.hashToString(12, rawPassword.toCharArray());
    }

    public boolean isValidUser(String username, String rawPassword) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); // Thay thế bằng loại driver của bạn

            Connection connection;
            connection = DBContext.getConnection();

            String query = "SELECT password FROM customers WHERE username = ?";
            PreparedStatement statement;
            statement = connection.prepareStatement(query);
            statement.setString(1, username);

            ResultSet resultSet;
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String hashedPassword = resultSet.getString("password");

                resultSet.close();
                statement.close();
                connection.close();

                // So sánh mật khẩu băm
                BCrypt.Verifyer verifyer = BCrypt.verifyer();
                BCrypt.Result result = verifyer.verify(rawPassword.toCharArray(), hashedPassword);

                return result.verified; // Trả về true nếu mật khẩu đúng, ngược lại là false
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return false; // Không tìm thấy tài khoản hoặc có lỗi
    }
}
