/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

/**
 *
 * @author Hoang
 */
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;

public class CategoryDAO implements DAOInterface<Category> {
// Thêm một đối tượng Category vào cơ sở dữ liệu
    @Override
    public void add(Category category) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {
            conn = DBContext.getConnection();
            String sql = "INSERT INTO ProductCategories (category_name, image_url) VALUES (?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, category.getCategoryName());
            stmt.setString(2, category.getImageUrl());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBContext.close(conn, stmt, null);
        }
    }

    // Cập nhật một đối tượng Category trong cơ sở dữ liệu
    @Override
    public void update(Category category) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {
            conn = DBContext.getConnection();
            String sql = "UPDATE ProductCategories SET category_name = ?, image_url = ? WHERE category_id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, category.getCategoryName());
            stmt.setString(2, category.getImageUrl());
            stmt.setInt(3, category.getCategoryId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBContext.close(conn, stmt, null);
        }
    }

    // Xóa một đối tượng Category khỏi cơ sở dữ liệu dựa trên ID
    @Override
    public void delete(int categoryId) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {
            conn = DBContext.getConnection();
            String sql = "DELETE FROM ProductCategories WHERE category_id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, categoryId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBContext.close(conn, stmt, null);
        }
    }

    // Lấy tất cả các đối tượng Category từ cơ sở dữ liệu
    @Override
    public List<Category> getAll() throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Category> categories = new ArrayList<>();
        
        try {
            conn = DBContext.getConnection();
            String sql = "SELECT * FROM ProductCategories";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("category_id"));
                category.setCategoryName(rs.getString("category_name"));
                category.setImageUrl(rs.getString("image_url"));
                categories.add(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBContext.close(conn, stmt, rs);
        }
        
        return categories;
    }

    // Lấy một đối tượng Category dựa trên ID
    @Override
    public Category getById(int categoryId) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Category category = null;
        
        try {
            conn = DBContext.getConnection();
            String sql = "SELECT * FROM ProductCategories WHERE category_id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, categoryId);
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                category = new Category();
                category.setCategoryId(rs.getInt("category_id"));
                category.setCategoryName(rs.getString("category_name"));
                category.setImageUrl(rs.getString("image_url"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBContext.close(conn, stmt, rs);
        }
        
        return category;
    }
}