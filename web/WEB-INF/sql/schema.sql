-- Tạo cơ sở dữ liệu "ogani"
CREATE DATABASE ogani;

-- Sử dụng cơ sở dữ liệu "ogani"
USE ogani;

-- Tạo bảng Danh mục sản phẩm (ProductCategories)
CREATE TABLE ProductCategories (
    category_id INT PRIMARY KEY,
    category_name VARCHAR(50),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Thêm dữ liệu mẫu cho bảng Danh mục sản phẩm
INSERT INTO ProductCategories (category_id, category_name)
VALUES
    (1, 'Thực phẩm hữu cơ'),
    (2, 'Hoa quả'),
    (3, 'Rau cải'),
    (4, 'Sản phẩm sữa');

-- Tạo bảng Sản phẩm (Products)
CREATE TABLE Products (
    product_id INT PRIMARY KEY,
    product_name VARCHAR(100),
    category_id INT,
    description TEXT,
    price DECIMAL(10, 2),
    stock_quantity INT,
    image_url VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (category_id) REFERENCES ProductCategories(category_id) ON DELETE CASCADE
);

-- Thêm dữ liệu mẫu cho bảng Sản phẩm
INSERT INTO Products (product_id, product_name, category_id, description, price, stock_quantity, image_url)
VALUES
    (1, 'Bắp ngọt hữu cơ', 1, 'Bắp ngọt hữu cơ sạch, ngon', 2.99, 100, 'img/latest-product/lp-1.jpg'),
    (2, 'Lê xanh', 2, 'Lê xanh tươi ngon', 1.99, 200, 'img/latest-product/lp-2.jpg'),
    (3, 'Cà rốt hữu cơ', 3, 'Cà rốt hữu cơ tươi ngon', 3.49, 150, 'img/latest-product/lp-3.jpg'),
    (4, 'Sữa tươi hữu cơ', 4, 'Sữa tươi hữu cơ 100%', 4.99, 50, 'img/latest-product/lp-3.jpg');


-- Tạo bảng Khách hàng (Customers)
CREATE TABLE Customers (
    customer_id INT PRIMARY KEY,
    username VARCHAR(50),
    password VARCHAR(255),
    email VARCHAR(255),
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    address VARCHAR(255),
    phone_number VARCHAR(20),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Thêm dữ liệu mẫu cho bảng Khách hàng
INSERT INTO Customers (customer_id, username, password, email, first_name, last_name, address, phone_number)
VALUES
    (1, 'customer1', 'hashed_password_1', 'customer1@example.com', 'John', 'Doe', '123 Main St', '555-1234'),
    (2, 'customer2', 'hashed_password_2', 'customer2@example.com', 'Jane', 'Smith', '456 Elm St', '555-5678');

-- Tạo bảng Đơn hàng (Orders)
CREATE TABLE Orders (
    order_id INT PRIMARY KEY,
    customer_id INT,
    order_date DATE,
    status VARCHAR(20),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (customer_id) REFERENCES Customers(customer_id) ON DELETE CASCADE
);

-- Thêm dữ liệu mẫu cho bảng Đơn hàng
INSERT INTO Orders (order_id, customer_id, order_date, status)
VALUES
    (1, 1, '2023-10-15', 'Đang xử lý'),
    (2, 2, '2023-10-16', 'Đã giao hàng');

-- Tạo bảng Chi tiết đơn hàng (OrderDetails)
CREATE TABLE OrderDetails (
    order_detail_id INT PRIMARY KEY,
    order_id INT,
    product_id INT,
    quantity INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (order_id) REFERENCES Orders(order_id) ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES Products(product_id) ON DELETE CASCADE
);

-- Thêm dữ liệu mẫu cho bảng Chi tiết đơn hàng
INSERT INTO OrderDetails (order_detail_id, order_id, product_id, quantity)
VALUES
    (1, 1, 1, 3),
    (2, 2, 2, 5);

-- Tạo bảng Đánh giá sản phẩm (ProductReviews)
CREATE TABLE ProductReviews (
    review_id INT PRIMARY KEY,
    customer_id INT,
    product_id INT,
    rating INT,
    review_text TEXT,
    review_date DATE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (customer_id) REFERENCES Customers(customer_id) ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES Products(product_id) ON DELETE CASCADE
);

-- Thêm dữ liệu mẫu cho bảng Đánh giá sản phẩm
INSERT INTO ProductReviews (review_id, customer_id, product_id, rating, review_text, review_date)
VALUES
    (1, 1, 1, 5, 'Sản phẩm rất tốt!', '2023-10-15'),
    (2, 2, 2, 4, 'Lê xanh ngon nhưng hơi cứng', '2023-10-16');

-- Tạo bảng Quản trị viên (Admins)
CREATE TABLE Admins (
    admin_id INT PRIMARY KEY,
    username VARCHAR(50),
    password VARCHAR(255),
    admin_name VARCHAR(50),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Thêm dữ liệu mẫu cho bảng Quản trị viên
INSERT INTO Admins (admin_id, username, password, admin_name)
VALUES
    (1, 'admin1', 'admin_password_1', 'Admin A'),
    (2, 'admin2', 'admin_password_2', 'Admin B');

-- Xóa cơ sở dữ liệu "ogani" cùng với tất cả các bảng
-- DROP DATABASE ogani; 